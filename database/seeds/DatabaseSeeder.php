<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $groups = ['Foodstuff', 'Detergents', 'Stationery'];
        \App\Supplier::insert([
            ['name' => 'RIDOMIC ENTERPRISES', 'phonenumber' => '0713642175'],
            ['name' => 'MANESSA CONTRACTORS LTD', 'phonenumber' => '0713642174'],
            ['name' => 'COMPWHIZ TECH SOLUTIONS', 'phonenumber' => '0713642173'],
            ['name' => 'DISCOVERY LIFE SPRINGS', 'phonenumber' => '0713642172'],
        ]);
        foreach ($groups as $group) {
            \App\Group::create([
                'group' => $group,
            ]);
        };
        \App\Item::insert([
            ['name' => 'Rice special of 2kg', 'unit' => 'Kg', 'group_id' => 1],
            ['name' => 'Rice Ordinary', 'unit' => '50 Kg Bag', 'group_id' => 1],
            ['name' => 'Maize Flour', 'unit' => 'Bale', 'group_id' => 1],
            ['name' => 'Wheat flour', 'unit' => 'Bale', 'group_id' => 1],
            ['name' => 'Self-raising flour', 'unit' => 'Bale', 'group_id' => 1],
            ['name' => 'Sugar (50 kg bag)', 'unit' => 'Bag', 'group_id' => 1],
            ['name' => 'Salt (1kg)', 'unit' => 'Pkt', 'group_id' => 1],
            ['name' => 'Margarine (10kg)', 'unit' => 'Carton', 'group_id' => 1],
            ['name' => 'Bread (400G)', 'unit' => 'Loaves', 'group_id' => 1],
            ['name' => 'Jam 1kg Tin', 'unit' => 'tin', 'group_id' => 1],
            ['name' => 'Royco (1kg tin)', 'unit' => '1Kg tin', 'group_id' => 1],
            ['name' => 'Cooking Oil (20 ltrs)', 'unit' => 'Jerrycan', 'group_id' => 1],
            ['name' => 'Dried Beans (90kg bag)', 'unit' => 'Bag', 'group_id' => 1],
            ['name' => 'Tea Leaves (500g)', 'unit' => 'Pkt', 'group_id' => 1],
            ['name' => 'Green Grams dried in 90kg bag', 'unit' => 'Bag', 'group_id' => 1],
            ['name' => 'Dry maize (90 kg bag)', 'unit' => 'Bag', 'group_id' => 1],
            ['name' => 'Wimbi (flour)', 'unit' => 'Kg', 'group_id' => 1],
            ['name' => 'Blue Band (1Kg tin)', 'unit' => 'tin', 'group_id' => 1],
            ['name' => 'All Purpose Wheat Flour', 'unit' => 'Bale', 'group_id' => 1],
            ['name' => 'Cooking Fat (17kgtin)', 'unit' => 'Tin', 'group_id' => 1],
            ['name' => 'Ndengu (90kg bag)', 'unit' => 'Bag', 'group_id' => 1],
            ['name' => 'Ground Nuts', 'unit' => 'Packets', 'group_id' => 1],
            ['name' => 'Yellow Beans (90kg Bag)', 'unit' => 'Bag', 'group_id' => 1],
            ['name' => 'Yeast ( 100gm)', 'unit' => 'Packets', 'group_id' => 1],
            ['name' => 'Baked Beans (420gm)', 'unit' => 'Tin', 'group_id' => 1],
            ['name' => 'Mandazi Flour', 'unit' => 'Bale', 'group_id' => 1],
            ['name' => 'Pasta (500gm)', 'unit' => 'Tin', 'group_id' => 1],
        ]);

        \App\Item::insert([
            ['name' => 'Photocopying/Printing Papers, 80gsm, A4, White', 'unit' => 'Ream', 'group_id' => 3],
            ['name' => 'Photocopying/Printing Papers, 80gsm, A4, Pink', 'unit' => 'Ream', 'group_id' => 3],
            ['name' => 'Photocopying/Printing Papers, 80gsm, A3, White', 'unit' => 'Ream', 'group_id' => 3],
            ['name' => 'Onion skin Paper, 100gms, A4, Cream', 'unit' => 'Ream', 'group_id' => 3],
            ['name' => 'Onion skin Paper, 100gms, A4, Blue', 'unit' => 'Ream', 'group_id' => 3],
            ['name' => 'Onion skin Paper, 100gms, A4, White', 'unit' => 'Ream', 'group_id' => 3],

        ]);

    }
}
