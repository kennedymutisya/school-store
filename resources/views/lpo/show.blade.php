@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Item: {{ $item->name }}</h4>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-row">
                                        <div class="round align-self-center round-success">
                                            <i class="ti-id-badge"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h3 class="m-b-0">{{ $item->received->sum('quantity') }}</h3>
                                            <h5 class="text-muted m-b-0">Received</h5></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-row">
                                        <div class="round align-self-center round-success">
                                            <i class="ti-id-badge"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h3 class="m-b-0">{{ $item->issued->sum('quantity') }}</h3>
                                            <h5 class="text-muted m-b-0">Issued</h5></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-row">
                                        <div class="round align-self-center round-success">
                                            <i class="ti-id-badge"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h3 class="m-b-0">{{ $item->received->sum('quantity') - $item->issued->sum('quantity') }}</h3>
                                            <h5 class="text-muted m-b-0">In-Stock</h5></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Issued {{ $item->name }} </h4>
                    <issue :issues="{{ $item->issued }}"></issue>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Received {{ $item->name }} </h4>
                    <receive :items="{{ $item->received }}"></receive>

                </div>
            </div>

        </div>
    </div>

@endsection
