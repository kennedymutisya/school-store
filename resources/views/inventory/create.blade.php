@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Item</h4>
                    <div class="col-md-6">
                        <additem :groupings="{{ \App\Group::all() }}"></additem>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Items</h4>
                    <items :allitems="{{ $items }}"></items>
                </div>
            </div>

        </div>
    </div>

@endsection
