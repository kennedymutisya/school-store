<html>
<head>
    <title>GOODS RECEIPT VOUCHER</title>
    <script language="JavaScript" type="text/javascript">
        /*setTimeout("window.print();", 10000);*/
    </script>
    <style>
        body {
            padding: 0px;
            margin: 0px;
            font-size: 12px;
        }

        table.data {
            font-family: Verdana;
            font-size: 17px;
            empty-cells: show;
            border: 1px solid #000;
            border-collapse: collapse;
            border-spacing: 0.5rem;
            empty-cells: show;
        }

        table.purchase {
            font-family: "Times New Roman";
            font-size: 17px;
            empty-cells: show;
            border: none;
            /*padding-left: 10px !important;*/

            margin: 24px !important;
            border-collapse: collapse;
            border-spacing: 0.5rem;
        }

        table.data td {
            border: 1px solid black;
        }

        table.data td.header {
            background-color: #EDECEB;
            font-size: medium;
        }

        table.data td.abottom {
            vertical-align: bottom;
            font-size: 10px;
        }

        span.title {
            font-size: 14px;
            font-weight: bold;
        }

        footer {
            position: fixed;
            bottom: 60px;
            left: 0px;
            right: 0px;
            height: 50px;
        }

        .pass {
            /*background-color: black !important;*/
            color: black !important;
            padding: 2px;
            font-weight: bold;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 0px;
                padding: 0px;
            }
        }

        @media screen {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 5px;
                padding: 5px;
            }
        }


    </style>

</head>
<body>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr>
        <td colspan="9">

            <table width="100%" border=0 cellspacing="0" cellpadding="1" class="data">

                <tr>
                    <td rowspan="6">
                        <img src="{{ asset('makinduHigh.PNG') }}" width="90px">
                    </td>
                </tr>
                <?php /** @var \App\Settings $settings */ ?>
                <tr>
                    <td valign="top" colspan="2">
                        <span class="title">MAKINDU HIGH SCHOOL</span>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" class="title"><b>ADDRESS : P.O BOX 28 &mdash; 90138, MAKINDU.</b></td>
                </tr>

                <tr>
                    <td colspan="2" class="title"><b>Tel : 0713 642 175</b></td>
                </tr>

                <tr>
                    <td colspan="2" class="title"><b>Vision : </b></td>
                </tr>

                <tr>
                    <td colspan="4" class="title"><strong>&nbsp;GOODS RECEIPT VOUCHER</strong></td>
                </tr>

            </table>

        </td>
    </tr>
</table>
<table cellpadding="2" cellspacing="0" width="100%" class="purchase">
    <tr>
        <td align="center"><h1>GOODS RECEIPT VOUCHER</h1></td>
    </tr>
</table>
<table cellpadding="2" cellspacing="0" width="100%" class="purchase">
    <tr>
        <td nowrap class="header" colspan="2">DEPT:
            <span class="pass"> <strong>................................................</strong></span>
        </td>
    </tr>
    <tr>
        <td nowrap class="header" colspan="2">
            <p><em><strong>Received the items listed below from (Source):</strong></em>
                <strong>{{$items->first()->supplier->name}}</strong></p>
        </td>
    </tr>
    <tr>
        <td nowrap class="header">
            <p><strong>LPO NO.: {{$items->first()->lpo_id ?? ''}}</strong></p>
        </td>
        <td nowrap class="header">
            <p>
                <strong>Date.: {{$items->first()->date ? \Carbon\Carbon::parse($items->first()->date)->toFormattedDateString() : '' }}</strong>
            </p>
        </td>
    </tr>
</table>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr nobr="true">
        <td nowrap class="header">Item No</td>
        <td nowrap class="header">Item Description</td>
        <td nowrap class="header">Units</td>
        <td nowrap class="header">Quantity</td>
        <td nowrap class="header">Value</td>
        <td nowrap class="header">Remarks/Purpose</td>
    </tr>
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->item->name }}</td>
            <td>{{ $item->item->unit}}</td>
            <td>{{ $item->quantity}}</td>
            <td>{{ $item->quantity}}</td>
            <td></td>
        </tr>
    @endforeach
</table>

<table cellpadding="2" cellspacing="0" width="100%" style="padding-top: 60px !important;margin-top: 60px !important;">
    <tr nobr="true">
        <td>Order No: <strong>................................</strong></td>
        <td>Date: <strong>.......................</strong></td>
        <td>Invoice No.: <strong>.......................</strong></td>
        <td>Date: <strong>.......................</strong></td>
    </tr>
    <tr>
        <td colspan="4">
            <p><strong>I certify that the quantities received have been taken on charge.</strong></p>
        </td>
    </tr>

</table>
<table cellpadding="2" cellspacing="0" width="100%" style="padding-top: 60px !important;margin-top: 60px !important;">
    <tr nobr="true">
        <td colspan="2">Vote Head/Account No: <strong>................................</strong></td>

    </tr>
    <tr nobr="true">
        <td>Receiving Officer: <strong>........................................................</strong></td>
        <td>Designation: <strong>........................................................</strong></td>
    </tr>
    <tr nobr="true">
        <td>Signature: <strong>........................................................</strong></td>
        <td>Date: <strong>........................................................</strong></td>
    </tr>

</table>


</body>
</html>
