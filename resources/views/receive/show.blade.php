@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Group: {{ $group->group }}</h4>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-row">
                                        <div class="round align-self-center round-success">
                                            <i class="ti-id-badge"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h3 class="m-b-0">40</h3>
                                            <h5 class="text-muted m-b-0">Total Items</h5></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-row">
                                        <div class="round align-self-center round-success">
                                            <i class="ti-wallet"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h3 class="m-b-0">70</h3>
                                            <h5 class="text-muted m-b-0">In-Stock</h5></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-row">
                                        <div class="round align-self-center round-success">
                                            <i class="ti-wallet"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h3 class="m-b-0">70</h3>
                                            <h5 class="text-muted m-b-0">Issued this Week</h5></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                <span>
                    <h4 class="card-title">{{ $group->group }} Items</h4>
                    <button type="button" class="btn btn-dark d-none d-lg-block m-l-15 pull-right" data-toggle="modal"
                            data-target=".bs-example-modal-lg">
                        <i class="fa fa-plus-circle"></i> Create New
                    </button>
                </span>
                    <items :allitems="{{ $group->items }}"></items>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Received {{ $group->group }}</h4>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Issue {{ $group->group }}</h4>
                </div>
            </div>

        </div>
    </div>

@endsection
