@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Receive Items</h4>
                    <receiveitems :items="{{ \App\Item::all() }}"
                                  :suppliers="{{ \App\Supplier::all() }}"></receiveitems>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Received Items</h4>
                    <receive :items="{{ \App\Receive::with(['supplier','item'])->get() }}"></receive>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Goods Receipt Voucher's</h4>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Contractor</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($supplier as $key=>$value)
                            <tr>
                                <td scope="row">{{ $key }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @foreach($value as $item)
                                <tr>
                                    <td></td>
                                    <td scope="row">
                                        {{ $item->first()->supplier->name }}
                                    </td>
                                    <td>{{ count($item) }} Items Delivered</td>
                                    <td>
                                        <div class="btn-group">
                                            <a title="Print Goods Receipt Voucher" href="{{ url('printgoodrecipt/'.$key.'/'.$item->first()->supplier_id.'/'.$item->first()->lpo_id) }}"
                                               class="btn btn-dark d-none d-lg-block m-l-15">
                                                <i class="fa fa-print"></i>
                                            </a>

                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                    {{--                    <receive :items="{{ \App\Receive::with(['supplier','item'])->get() }}"></receive>--}}
                </div>
            </div>

        </div>
    </div>

@endsection
