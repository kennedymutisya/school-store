@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Ledger</h4>
                    <addgroup></addgroup>
                    

                </div>
            </div>

        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Ledgers</h4>
                    <groups :groupings="{{ \App\Group::all() }}"></groups>


                </div>
            </div>

        </div>
    </div>

@endsection
