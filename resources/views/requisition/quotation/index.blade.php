<html>
<head>
    <title>REQUEST FOR QUOTATION</title>
    <script language="JavaScript" type="text/javascript">
        /*setTimeout("window.print();", 10000);*/
    </script>
    <style>
        body {
            padding: 0px;
            margin: 0px;
            font-size: 12px;
        }

        table.data {
            font-family: Verdana;
            font-size: 17px;
            empty-cells: show;
            border: 1px solid black;
            border-collapse: collapse;
            border-spacing: 0.5rem;
            empty-cells: show;
        }

        table.purchase {
            font-family: "Times New Roman";
            font-size: 17px;
            empty-cells: show;
            border: none;
            /*padding-left: 10px !important;*/

            margin: 24px !important;
            border-collapse: collapse;
            border-spacing: 0.5rem;
        }

        table.data td {
            border: 1px solid black;
        }

        table.data td.header {
            background-color: #EDECEB;
            font-size: medium;
        }

        table.data td.abottom {
            vertical-align: bottom;
            font-size: 10px;
        }

        span.title {
            font-size: 14px;
            font-weight: bold;
        }

        footer {
            position: fixed;
            bottom: 60px;
            left: 0px;
            right: 0px;
            height: 50px;
        }

        .pass {
            /*background-color: black !important;*/
            color: black !important;
            padding: 2px;
            font-weight: bold;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 0px;
                padding: 0px;
            }
        }

        @media screen {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 5px;
                padding: 5px;
            }
        }


    </style>

</head>
<body>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr>
        <td colspan="9">

            <table width="100%" border=0 cellspacing="0" cellpadding="1" class="data">

                <tr>
                    <td rowspan="6">
                        <img src="{{ asset('makinduHigh.PNG') }}" width="90px">
                    </td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <strong>MAKINDU BOYS HIGH SCHOOL</strong>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" class="title"><b>ADDRESS : P.O BOX 28 &mdash; 90138, MAKINDU.</b></td>
                </tr>

                <tr>
                    <td colspan="2" class="title"><b>Tel : 0713 642 175</b></td>
                </tr>

                <tr>
                    <td colspan="2" class="title"><b>Vision : </b></td>
                </tr>

                <tr>
                    <td colspan="4" class="title"><strong>&nbsp;REQUEST FOR QUOTATION</strong></td>
                </tr>

            </table>

        </td>
    </tr>
</table>
<table cellpadding="2" cellspacing="0" width="100%" class="purchase">
    <tr style="font-size: 24px">
        <td nowrap class="header">TO:
            <span class="pass">COMPWHIZ TECHNOLOGY SOLUTIONS,<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P.O BOX 132 &mdash; 90138, MAKINDU</span>
        </td>
        <td nowrap class="header">FROM:
            <span class="pass">MAKINDU BOYS HIGH SCHOOL,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P.O BOX 28 &mdash; 90138, MAKINDU</span>
        </td>
    </tr>
    <tr style="font-size: 24px;padding-top: 80px !important;">
        <td nowrap class="header">Quotation No:
            <span class="pass">MKD/0009/2016-2018/01</span>
        </td>
        <td nowrap class="header">Date:
            <span class="pass">{{ \Carbon\Carbon::today()->toFormattedDateString() }}</span>
        </td>
    </tr>
</table>
<table cellpadding="2" cellspacing="0" width="100%" class="purchase">
    <tr>
        <td nowrap class="header" style="font-size: 24px">
            <p>The School named above requests that you submit a quotation for the provision of the under-listed
                goods,<br>
                works or services. The quotations must be submitted in the form and manner specified below: </p>
            <p> The quotations must be supplied to the school within <strong>7 Days</strong>.</p>
            <p> Incomplete quotations or those received after the deadline will automatically be rejected. </p>
            <p> This request for quotation is not an order. An official order will be issued to the selected supplier
                <br>immediately the selection process is completed</p>
        </td>
    </tr>

</table>

<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr nobr="true">
        <td nowrap class="header" colspan="4"
            style="border-top: transparent 1px solid !important;border-left: transparent 1px solid !important;background-color: transparent !important;"></td>
        <td nowrap class="header" colspan="6" align="center"> To be completed by supplier</td>
    </tr>
    <tr nobr="true">
        <td nowrap class="header">Item No.</td>
        <td nowrap class="header">Item Description</td>
        <td nowrap class="header">Units</td>
        <td nowrap class="header">Quantity</td>
        <td class="header">Unit Price &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="header">Days <br>to Deliver</td>
        <td class="header">Discount</td>
        <td class="header">Brand Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="header">Country <br> of Origin</td>
        <td nowrap class="header">Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>

    @foreach($requistion->items as $requistio)
        <tr style="font-size: 14px">
            <td valign="top">{{ $loop->iteration }}</td>
            <td valign="top">{{ mb_strtoupper($requistio->item) }}</td>
            <td valign="top">{{ $requistio->Unit }}</td>
            <td valign="top">{{ $requistio->quantity }}</td>
            <td>
                <br>
                <br>
                <br>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                &nbsp;<br>
            </td>
        </tr>
    @endforeach
</table>
<table cellpadding="2" cellspacing="0" width="100%" class="purchase"
       style="padding-top: 100px !important;margin-top: 100px !important;">
    <tr>
        <td nowrap class="header">Supplier's Signature: ___________________________________________</td>
        <td nowrap class="header">Date: ___________________________________________</td>
    </tr>


</table>
<table cellpadding="2" cellspacing="0" width="100%" class="purchase"
       style="padding-top: 50px !important;margin-top: 50px !important;">
    <tr>
        <td nowrap class="header" colspan="3"><strong>FOR OFFICIAL USE</strong></td>
    </tr>
    <tr>
        <td>Opened by:
            <br><br>...............................................................
        </td>
        <td>Designation: <br><br> _______________________________________________</td>
        <td>Date/Time: <br> <br>_______________________________________________</td>
    </tr>
    <tr>
        <td style="margin-top: 50px !important;padding-top: 50px !important;">Checked by: <br> <br>................................................................
        </td>
        <td style="margin-top: 50px !important;padding-top: 50px !important;">Designation: <br> <br>_______________________________________________
        </td>
        <td style="margin-top: 50px !important;padding-top: 50px !important;">Date/Time: <br> <br>_______________________________________________
        </td>
    </tr>

</table>
<footer>
    <table cellpadding="2" cellspacing="0" width="100%" class="purchase">

        <tr>
            <td>
                <em>ORIGINAL: TO SUPPLIER</em><br>
                <em>DUPLICATE</em><br>
                <em>TRIPLICATE</em><br>
            </td>
        </tr>

    </table>
</footer>
</body>
</html>
