<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LPO extends Model
{
    protected $table = 'lpo';
    protected $guarded = [];
    public function items()
    {
        return $this->hasMany(lpoitem::class,'lpo_id','id');
    }

    public function contractors()
    {
        return $this->hasOne(Supplier::class,'id','contractor');
    }
}
