<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    public function group()
    {
        return $this->belongsTo(Item::class,'id', 'group_id');
    }

    public function items()
    {
        return $this->hasMany(Item::class, 'group_id', 'id');
    }

    public function received()
    {
        return $this->hasManyThrough(Receive::class,Item::class);
    }

    public function issued()
    {
        return $this->hasManyThrough(Issue::class,Item::class,'group_id','item','id','id');

    }
}
