<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lpoitem extends Model
{
    protected $guarded = [];
    protected $appends = ['received'];

    public function items()
    {
        return $this->hasOne(Item::class,'id','item');
    }

    public function getReceivedAttribute()
    {
        return '';
    }
}
