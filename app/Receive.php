<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receive extends Model
{
    public function supplier()
    {
        return $this->hasOne(Supplier::class,'id','supplier_id');
    }

    public function item()
    {
        return $this->hasOne(Item::class, 'id', 'item_id');
    }
}
