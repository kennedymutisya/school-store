<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requistion extends Model
{
    public function items()
    {
        return $this->hasMany(RequisitionItem::class,'requisition_id','id');
   }
}
