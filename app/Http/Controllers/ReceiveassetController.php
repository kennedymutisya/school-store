<?php

namespace App\Http\Controllers;

use App\Receiveasset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReceiveassetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Receiveasset  $receiveasset
     * @return \Illuminate\Http\Response
     */
    public function show(Receiveasset $receiveasset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Receiveasset  $receiveasset
     * @return \Illuminate\Http\Response
     */
    public function edit(Receiveasset $receiveasset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Receiveasset  $receiveasset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Receiveasset $receiveasset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Receiveasset  $receiveasset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Receiveasset $receiveasset)
    {
        //
    }
}
