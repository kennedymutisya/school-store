<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{

    public function items()
    {
        return $this->hasOne(Item::class, 'id', 'item');
    }


}
