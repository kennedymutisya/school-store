<?php

/*
|--------------------------------------------------------------------------
| TODO:
|--------------------------------------------------------------------------
|
Public Procurement and Disposal Act 2005, Public Procurement and Disposal Regulations 2006,
PPOA Procurement and Disposal General Manual
 Schools and Colleges Procurement Manual
Public Procurement and Disposal General Manual
Suppliers’ Approved List (Regulation 8 (3) or Regulation 23 of the PPD Regulations)

Registration of Suppliers
Pre-qualification of Suppliers (Regulation 23 of the PPDR 2006)
LSO/LPO
Goods Receipt Voucher (Appendix J)
Inspection and Verification Certificate
stores ledger
Stores Catalogues
Counter Requisition and Issue Voucher

Re-order Level System (fixed order quantity, variable order intervals);
Periodic Review System (fixed order interval, variable order quantity);
Demand Driven Lean Supply Systems (orders placed in the precise quantity and time required for a specific projects or assignments)
Economic Order Quantity (order what is economical to produce or deliver).
|
*/

//dd(\App\Requistion::find(1)->load('items'));;

Route::get('tests', function () {

    dd(\App\Group::with('received.item', 'received.supplier')->get());

});

Route::get('/', function () {
    return redirect('lpo');
});

Route::resource('groups', 'GroupController');

Route::resource('items', 'ItemController');

Route::get('printrequisition/{requistion}', 'RequistionController@print');
Route::get('printlpo/{lpo}', 'LPOController@print');
Route::get('printgoodrecipt/{date}/{supplier}/{lpo}', 'ReceiveController@print');

Route::resource('requisition', 'RequistionController');

Route::resource('receive', 'ReceiveController');

Route::resource('suppliers', 'SupplierController');

Route::resource('issue', 'IssueController');

Route::get('receive_inventory', 'InventoryController@receive');

Route::post('receive_inventory', 'InventoryController@receiveInv');

Route::resource('lpo', 'LPOController');

Route::resource('inventory', 'InventoryController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
