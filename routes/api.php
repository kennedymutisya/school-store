<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('ContractorLpos/{supplier}', function ($supplier) {

    $lpos = \App\Supplier::find($supplier)->load('lpo');

    return response()->json($lpos['lpo']);

});
Route::get('lpoItems/{lpo}', function ($lpo) {

    $lpos = \App\LPO::find($lpo)->load('items.items');

    return response()->json($lpos['items']);

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
